Johnnie Walker
==============

Generate Secrets and Vaults for other ansible-based repositories

Read the Makefile for more information on the types of plays included


## Getting Started

1. get password
log into passpack and get the ansible-vault password
look for *Ansible Vault* (johnnie-walker) password

2. clone repo
> git clone git@bitbucket.org:mobileembrace/johnnie-walker.git
> cd johnnie-walker

3. run repo
> make secrets-ask-pass
> \# enter password from item#1 when prompted



## Adding new secrets

To add new secrets you'll need to `make edit-secrets` to add any new vault-files required in
any new repositories

Create key-<repo>.yml and associated Make entry providing *app_name* and *repo_name*