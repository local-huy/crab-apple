Makers Mark
===========

This repo contains ansible playbooks and roles that can be used and imported across
other repositories.

Each piece of automation should have tests associated with it to validate changes.

This repo is used for creating new project with roles: "new_project" to created a pair public/private key and store in secret.yml file, also with vault_password which was used for Johnnie Walker project to create vault.